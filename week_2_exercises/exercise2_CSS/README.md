## Week 2 CSS exercise

Style your website using:

1. At least a element, id or class selector 

2. At least one color styling (e.g. color: red) and one text styling (e.g. line-height: 1.5em)

3. Use margins, paddings and borders

4. Use a HTML5 semantic element like article
