## Week 2 HTML exercise

Extend your travelling website:

1. Use at least one image element (<img>)

2. Use at least one link element (<a href=“..”>)

3. Use at least one input element: a text input (e.g. <input type=“text”>),
 a checkbox input (e.g. <input type=“checkbox”>) or a radio button (e.g. <input type=“radio”>)
