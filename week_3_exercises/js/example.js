function showComment(country) {
    var comment = document.getElementById("comment" + country).value;
    document.getElementById("showComment" + country).innerHTML = comment;
    document.getElementById("divComment" + country).style.display = "none";
}

function showGallery(country) {
    document.getElementById("gallery" + country).style.display = "block";
}

function hideGallery(country) {
    document.getElementById("gallery" + country).style.display = "none";
}