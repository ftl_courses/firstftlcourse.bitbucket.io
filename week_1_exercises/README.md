# Week 1 Exercise
	1. Create a website page (index.html) with the theme of a travelling blog

	2. Use at least one heading tag(e.g. <h1></h1>, <h2></h2>)

	3. Use at least one ordered list (<ol></ol>) and an unordered list (<ul></ul>)

	4. Use at least one tag for text formatting (e.g. <b></b>, <i></i>)

	5. Write a comment in our code

	6. Use at least one HTML semantic element (e.g. <article> </article>)

Follow the instructions at Create_A_Web_Page_Using_Visual_Studio_Code.pdf to create a index.html file in Visual Studio Code.
!! Don’t forget to commit and push your code into bitbucket !! 

# Run the solution
Double-click on index.html. The default browser will open the file.  